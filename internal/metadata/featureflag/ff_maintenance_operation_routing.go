package featureflag

// MaintenanceOperationRouting enables routing logic that is specific to maintenance operations.
var MaintenanceOperationRouting = NewFeatureFlag("maintenance_operation_routing", false)
